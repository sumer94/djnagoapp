from django.conf.urls import url
from . import views
 
urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'add', views.add, name='add'),
    url(r'^getdata/', views.getData, name='getData'),
    url(r'^update/(\d+)/$', views.update, name='update'),
    url(r'^showdata/', views.showData),
    url(r'^delete/$', views.delete, name='deleteData'),
    url(r'^doUpdate/(\d+)', views.doUpdate, name='doUpdate'),
    url(r'^profile/$', views.profile, name='profile')

]

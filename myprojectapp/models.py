# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.



class Person(models.Model):
    name = models.CharField(max_length=30)
    city = models.CharField(max_length=30)
def get_field_values(self):
        return [field.value_to_string(self) for field in Person._meta.fields]

        pass